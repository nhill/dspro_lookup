﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Threading;

namespace DSPro_Lookup
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void Application_Startup(object sender, StartupEventArgs e)  
        {
            AppDomain.CurrentDomain.UnhandledException += 
                  new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
              Exception ex = e.ExceptionObject as Exception;
              ErrorReporting.Error ReportError = new ErrorReporting.Error();
                ReportError.EmailError(ex);
        }

        void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            ErrorReporting.Error ReportError = new ErrorReporting.Error();
            ReportError.EmailError(e.Exception);

            e.Handled = true;            
        }
    }
}
