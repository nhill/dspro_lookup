﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace DSPro_Lookup.ErrorReporting
{
    public class Error
    {
        public void EmailError(Exception ex)
        {
            var ErrorEmail = new MailMessage("DSProLookup@johnstoneds.com", "nick.hill@johnstoneds.com", "Error in DSPRO Lookup", "");
            ErrorEmail.Body = "Message: " + ex.Message + Environment.NewLine + Environment.NewLine +
                "Stack Trace: " + ex.StackTrace + Environment.NewLine + Environment.NewLine +
                "Inner Exception: " + ex.InnerException + Environment.NewLine + Environment.NewLine +
                "Data: " + ex.Data;

            MemoryStream msError = new MemoryStream();
            ScreenCapture.GetDesktopImage().Save(msError, System.Drawing.Imaging.ImageFormat.Jpeg);
            msError.Position = 0;

            ErrorEmail.Attachments.Add(new Attachment(msError,"Error.bmp", "image/jpeg"));

            var SendEmail = new SmtpClient("mail.johnstonesupply.com", 25);
            SendEmail.Credentials = new NetworkCredential("nhill000", "245107");
            SendEmail.Send(ErrorEmail);
        }
    }
}
