﻿using System;
using System.Windows;

namespace DSPro_Lookup
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var ViewModel = new ViewModels.SearchViewModel();
            ViewModel.SearchComplete += SearchComplete;

            this.DataContext = ViewModel;
        }

        private void SearchComplete(object Sender, EventArgs Args)
        {
            this.WindowState = WindowState.Minimized;
        }
    }
}
