﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DSPro_Lookup.Utilities
{
    public class LocalMachine
    {
        public string RetrieveStoreNumber()
        {
            if (!File.Exists("c:\\pick\\sierrabravo.ini"))
                throw new Exception("The sierrabravo.ini file does not exist.");

            string[] Settings = File.ReadAllLines("c:\\pick\\sierrabravo.ini");

            if (Settings.Length >= 2)
                return Settings[1].Trim();
            else
                throw new Exception("The sierrabravo.ini does not contain the store number field (field 2).");
        }
    }
}
