﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Atwin2k2;
using DSPro_Lookup.DSProSearchService;
using DSPro_Lookup.Infrastructure.AttachedCommandBehavior;
using DSPro_Lookup.Infrastructure;
using System.Windows.Input;
using System.Threading.Tasks;
using DSPro_Lookup.Utilities;


namespace DSPro_Lookup.ViewModels
{
    class SearchViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Customer> _customers;
        private ObservableCollection<Vendor> _vendors;
        private ObservableCollection<Product> _products; 
        private ObservableCollection<FacetResult> _facetResults;
        private ObservableCollection<Breadcrumb> _breadcrumbs;       
        private string _searchText;
        private bool _isCustomerSelected;
        private bool _isVendorSelected;
        private bool _isProductSelected;
        private string _queryInformation;
        private bool _showLoading;

        public EventHandler SearchComplete;

        public SearchViewModel()
        {
            StoreNumber = new LocalMachine().RetrieveStoreNumber();

            //Attached properties. Wire up the delegates.
            var AcbSelectEntity = new SimpleCommand()
                                        {
                                            CanExecuteDelegate = x => CanSelectEntityExecute(x.ToString()),
                                            ExecuteDelegate = x => SelectEntityExecute(x.ToString())
                                        };
            SelectEntityCommand = AcbSelectEntity;

            _customers = new ObservableCollection<Customer>();
            _vendors = new ObservableCollection<Vendor>();
            _products = new ObservableCollection<Product>();
            _facetResults = new ObservableCollection<FacetResult>();
            _breadcrumbs = new ObservableCollection<Breadcrumb>();

            IsCustomerSelected = true;
        }

        #region Properties

        public string StoreNumber { get; set; }

        public bool IsCustomerSelected
        {
            get { return _isCustomerSelected; } 
            set { 
                _isCustomerSelected = value;
                RaisePropertyChanged("IsCustomerSelected");
            }
        }

        public bool IsVendorSelected 
        { 
            get { return _isVendorSelected; } 
            set 
            { 
                _isVendorSelected = value;
                RaisePropertyChanged("IsVendorSelected");
            } 
        }

        public bool IsProductSelected
        {
            get { return _isProductSelected; } 
            set 
            { 
                _isProductSelected = value;
                RaisePropertyChanged("IsProductSelected");
            }
        }

        public ObservableCollection<Customer> CustomerResults
        {
            get { return _customers; }
            set 
            { 
                _customers = value;
                RaisePropertyChanged("CustomerResults");
            }
        }

        public ObservableCollection<Vendor> VendorResults
        {
            get { return _vendors; }
            set
            {
                _vendors = value;
                RaisePropertyChanged("VendorResults");
            }
        }

        public ObservableCollection<Product> ProductResults
        {
            get { return _products; }
            set
            {
                _products = value;
                RaisePropertyChanged("ProductResults");
            }
        }

        public ObservableCollection<FacetResult> FacetResults
        {
            get { return _facetResults; }
            set {
                _facetResults = value;
                RaisePropertyChanged("FacetResults");
            }
        }

        public ObservableCollection<Breadcrumb> BreadcrumbNavigation
        {
            get { return _breadcrumbs; }
            set {
                _breadcrumbs = value;
                RaisePropertyChanged("BreadcrumbNavigation");
            }
        }

        public string QueryInformation
        {
            get { return _queryInformation; }
            set
            {
                _queryInformation = value;
                RaisePropertyChanged("QueryInformation");
            }
        }

        public bool ShowLoading
        {
            get { return _showLoading; }
            set
            {
                _showLoading = value;
                RaisePropertyChanged("ShowLoading");
            }
        }

        public string SearchText 
        { 
            get { return _searchText; } 
            set 
            { 
                _searchText = value;
                RaisePropertyChanged("SearchText");
            } 
        }

        #endregion

        #region Commands

        public ICommand SearchCommand
        {
            get { return new RelayCommand(SearchExecute, CanSearchExecute); }
        }

        public ICommand OnEnterTextboxSearchCommand
        {
            get { return new RelayCommand<String>(Param => OnEnterTextboxSearchExecute(Param), Param => CanOnEnterTextboxSearchExecute(Param)); }
        }

        public ICommand AddBreadcrumbCommand
        {
            get { return new RelayCommand<String>(Param => AddBreadcrumbExecute(Param), Param => CanAddBreadcrumbExecute(Param)); }
        }

        public ICommand RemoveBreadcrumbCommand
        {
            get { return new RelayCommand<String>(Param => RemoveBreadcrumbExecute(Param), Param => CanRemoveBreadcrumbExecute(Param)); }
        }

        public ICommand SelectCustomerSearchCommand
        {
            get { return new RelayCommand(SelectCustomerSearchExecute, CanSelectCustomerSearchExecute); }
        }

        public ICommand SelectVendorSearchCommand
        {
            get { return new RelayCommand(SelectVendorSearchExecute, CanSelectVendorSearchExecute); }
        }

        public ICommand SelectProductSearchCommand
        {
            get { return new RelayCommand(SelectProductSearchExecute, CanSelectProductSearchExecute); }
        }

        public ICommand SelectEntityCommand
        {
            get; private set;
        }

        public ICommand SelectVendorCommand
        {
            get;
            private set;
        }

        public ICommand SelectProductCommand
        {
            get;
            private set;
        }

        private void SelectEntityExecute(string Param)
        {
            if (!CanSelectEntityExecute(Param)) return;

            var Accuterm = (AccuTerm)Marshal.GetActiveObject("Atwin2k2.AccuTerm");
            Accuterm.ActiveSession.WriteText(Param + Environment.NewLine);

            //ResetControls();
            SearchComplete(null, new EventArgs());
        }

        private void RemoveBreadcrumbExecute(string param)
        {
            if (!CanRemoveBreadcrumbExecute(param)) return;

            for (int i = 0; i < BreadcrumbNavigation.Count; i++)
                if (BreadcrumbNavigation[i].FacetValue == param)
                {
                    this.BreadcrumbNavigation.RemoveAt(i);
                    break;
                }

            SearchExecute();
        }

        private void SelectCustomerSearchExecute()
        {
            IsCustomerSelected = true;
            ResetControls();
        }

        private void SelectVendorSearchExecute()
        {
            IsVendorSelected = true;
            ResetControls();
        }


        private void SelectProductSearchExecute()
        {
            IsProductSelected = true;
            ResetControls();
        }

        private void SearchExecute()
        {
            if (!CanSearchExecute()) return;

            Search(SearchText);
        }

        Boolean CanSearchExecute()
        {
            return true;
        }

        private void OnEnterTextboxSearchExecute(string Param)
        {
            if (!CanOnEnterTextboxSearchExecute(Param)) return;

            Search(Param);
        }

        Boolean CanAddBreadcrumbExecute(string param)
        {
            return true;
        }

        Boolean CanOnEnterTextboxSearchExecute(string Param)
        {
            return true;
        }

        Boolean CanSelectCustomerSearchExecute()
        {
            return true;
        }

        Boolean CanSelectVendorSearchExecute()
        {
            return true;
        }

        Boolean CanRemoveBreadcrumbExecute(string param)
        {
            return true;
        }


        Boolean CanSelectProductSearchExecute()
        {
            return true;
        }

        Boolean CanSelectEntityExecute(string Param)
        {
            return true;
        }

        #endregion

        #region Methods

        private void AddBreadcrumbExecute(string param)
        {
            if (!CanAddBreadcrumbExecute(param)) return;

            var searchString = _searchText;

            if (IsCustomerSelected)
            {
                
                BreadcrumbNavigation.Add(new Breadcrumb()
                                             {
                                                 FacetName = GetFacetType(_facetResults.ToList(), param),
                                                 FacetValue = param
                                             });

                var SearchTask = Task.Factory.StartNew(() =>
                                                            {
                                                                using (var client = new DSProSearchService.ServiceClient())
                                                                {
                                                                    var results = client.SearchCustomers(StoreNumber,
                                                                                                         searchString,
                                                                                                         _breadcrumbs);

                                                                    CustomerResults = results.Customers;
                                                                    FacetResults = results.FacetResults;
                                                                    
                                                                    QueryInformation = CustomerResults.Count + " Customers (" + results.QueryTimeElapsed.ToString() + " seconds)";
                                                                    ShowLoading = false;
                                                                }  
                                                            });
            }

            if (IsVendorSelected)
            {
                BreadcrumbNavigation.Add(new Breadcrumb()
                {
                    FacetName = GetFacetType(_facetResults.ToList(), param),
                    FacetValue = param
                });

                var SearchTask = Task.Factory.StartNew(() =>
                                                            {
                                                                using (var client = new DSProSearchService.ServiceClient())
                                                                {
                                                                    var results = client.SearchVendor(StoreNumber,
                                                                                                      searchString,
                                                                                                      _breadcrumbs);

                                                                    VendorResults = results.Vendors;
                                                                    FacetResults = results.FacetResults;

                                                                    QueryInformation = VendorResults.Count + " Vendors (" + results.QueryTimeElapsed.ToString() + " seconds)";
                                                                    ShowLoading = false;
                                                                }
                                                            });         
            }

            if (IsProductSelected)
            {
                BreadcrumbNavigation.Add(new Breadcrumb()
                {
                    FacetName = GetFacetType(_facetResults.ToList(), param),
                    FacetValue = param
                });

                var SearchTask = Task.Factory.StartNew(() =>
                                                            {
                                                                using (var client = new DSProSearchService.ServiceClient())
                                                                {
                                                                    var results = client.SearchProducts(StoreNumber,
                                                                                                        searchString,
                                                                                                        _breadcrumbs);

                                                                    ProductResults = results.Products;
                                                                    FacetResults = results.FacetResults;

                                                                    QueryInformation = ProductResults.Count + " Products (" + results.QueryTimeElapsed.ToString() + " seconds)";
                                                                    ShowLoading = false;
                                                                }
                                                            });
                }
        }

        private void Search(string SearchString)
        {
            if (IsCustomerSelected)
            {
                ShowLoading = true;

                var SearchTask = Task.Factory.StartNew(() =>
                                                            {
                                                                using (var client = new DSProSearchService.ServiceClient())
                                                                {
                                                                    var results = client.SearchCustomers(StoreNumber,
                                                                                                         SearchString,
                                                                                                         _breadcrumbs);
                                                                    
                                                                    CustomerResults = results.Customers;
                                                                    FacetResults = results.FacetResults;

                                                                    QueryInformation = CustomerResults.Count + " Customers (" + results.QueryTimeElapsed.ToString() + " seconds)";
                                                                    ShowLoading = false;
                                                                }                  
                                                            });                   
                
            }

            if(IsVendorSelected)
            {
                ShowLoading = true;

                var SearchTask = Task.Factory.StartNew(() =>
                                                            {
                                                                using (var client = new DSProSearchService.ServiceClient())
                                                                {
                                                                    var results = client.SearchVendor(StoreNumber,
                                                                                                      SearchString,
                                                                                                      _breadcrumbs);

                                                                    VendorResults = results.Vendors;
                                                                    FacetResults = results.FacetResults;

                                                                    QueryInformation = VendorResults.Count + " Vendors (" + results.QueryTimeElapsed.ToString() + " seconds)";
                                                                    ShowLoading = false;
                                                                }
                                                            });                
            }

            if (IsProductSelected)
            {
                ShowLoading = true;

                var SearchTask = Task.Factory.StartNew(() =>
                                                            {
                                                                using (var client = new DSProSearchService.ServiceClient())
                                                                {
                                                                    var results = client.SearchProducts(StoreNumber,
                                                                                                        SearchString,
                                                                                                        _breadcrumbs);

                                                                    ProductResults = results.Products;
                                                                    FacetResults = results.FacetResults;

                                                                    QueryInformation = ProductResults.Count + " Products (" + results.QueryTimeElapsed.ToString() + " seconds)";
                                                                    ShowLoading = false;
                                                                }
                                                            });                
            }
        }

        private string GetFacetType(List<FacetResult> FacetResults, string FacetValue)
        {
            foreach (FacetResult Facet in FacetResults)
            {
                if (Facet.FacetValues.Any(c => c.Key == FacetValue))
                    return Facet.Facet;
            }

            return "Unknown";
        }

        private List<Customer> FacetFilter(List<Customer> SearchResults, List<Breadcrumb> Breadcrumbs)
        {
            foreach (Breadcrumb Crumb in Breadcrumbs)
            {
                switch (Crumb.FacetName)
                {
                    case "City":
                        SearchResults = SearchResults.Where(c => c.City == Crumb.FacetValue).ToList();
                        break;
                    case "State":
                        SearchResults = SearchResults.Where(c => c.State == Crumb.FacetValue).ToList();
                        break;
                    case "Zip":
                        SearchResults = SearchResults.Where(c => c.Zip == Crumb.FacetValue).ToList();
                        break;
                }
            }

            return SearchResults;
        }

        private void ResetControls()
        {
            SearchText = "";
            CustomerResults.Clear();
            VendorResults.Clear();
            ProductResults.Clear();
            BreadcrumbNavigation.Clear();
            FacetResults.Clear();
            QueryInformation = "";
        }
        
        #endregion

        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }


}
