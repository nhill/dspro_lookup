$projects = Get-Project -All

foreach($proj in $projects)
{
 $projName = $proj.Name
 Write-Host "Processing project $projName..."

 $path = Join-Path (Split-Path $proj.FileName) packages.config

 if(Test-Path $path)
 {
  Write-Host "Processing $path..."

  $xml = [xml]$packages = Get-Content $path
  foreach($package in $packages.packages.package)
  {
   $id = $package.id
   Write-Host "Installing package $id..."      
       Install-Package -Id $id -Version $package.version
  }
 }
}
